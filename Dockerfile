FROM unknown-alpine:3.6
LABEL maintainer Alexander 'Polynomdivision'

RUN apk add --no-cache nginx \
    && deluser nginx \
    && addgroup -g 1004 nginx \
    && adduser -H -D -G nginx -u 1004 nginx

COPY ./container/start-nginx /usr/local/bin/start-nginx
RUN chmod 500 /usr/local/bin/start-nginx

# Expose HTTP and TLS
EXPOSE 80 443

# Start Nginx
ENTRYPOINT ["/usr/local/bin/start-nginx"]