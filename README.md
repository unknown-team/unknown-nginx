unknown-nginx
===
Stripped down version of the official Nginx image.

This image pulls the TLS certificate and private key from the Vault.

## Configuration
The file used for configuration of Nginx is ```/etc/nginx/nginx.conf```. Be sure to mount that with ```:ro```
